import React, {useState, useEffect} from 'react';

import { useHistory } from "react-router-dom";
import {
    Button,
    Container,
    Menu,
    Header
  } from 'semantic-ui-react';
  import { useSiteData } from '../contexts/SiteDataContext';

const Navbar = ({fixed}) => {

    const {activeItem, setActiveItem} = useSiteData();
    const history = useHistory();


    const handleItemClick = (e, { name }) => {
        setActiveItem(name);
        // console.log(currentUser)
        if (name === "home") {
          history.push(`/`);
        } else {
          history.push(`/${name}`);
        }
      };

      const handleButtonClick = (e, name) => {
        // setActiveButton({ activeItem: name });
        // console.log(name);
        // if (name === "login" && currentUser) {
        if (name === "login" ) {
        //   logout();
          history.push(`/login`);
        } else if (name === "home") {
          history.push(`/`);
        } else {
          history.push(`/${name}`);
        }
      };

    return (
            <Menu
                fixed={fixed ? 'top' : null}
                inverted={!fixed}
                pointing={!fixed}
                secondary={!fixed}
                size='massive'
                // style={{border: "3px solid green"}}
                // style={{border: "1px solid white"}}
                >
                    <Container>
                        <Menu.Item as='a' name = "home" onClick={handleItemClick} active={activeItem === 'home'}>
                            <Header inverted={!fixed} as='h3'>Home</Header>
                        </Menu.Item>
                        <Menu.Item as='a' name = "company" onClick={handleItemClick} active={activeItem === 'company'}>
                            <Header inverted={!fixed} as='h3'>Company</Header>
                        </Menu.Item>
                        <Menu.Item as='a' name = "services" onClick={handleItemClick} active={activeItem === 'services'}>
                            <Header inverted={!fixed} as='h3'>Services</Header>
                        </Menu.Item>
                        <Menu.Item as='a' name = "team" onClick={handleItemClick} active={activeItem === 'team'}>
                            <Header inverted={!fixed} as='h3'>Team</Header>
                        </Menu.Item>
                        <Menu.Item as='a' name = "contact" onClick={handleItemClick} active={activeItem === 'contact'}>
                            <Header inverted={!fixed} as='h3'>Contact</Header>
                        </Menu.Item>
                        <Menu.Item position='right'>
                        <Button as='a' onClick={(e)=>{handleButtonClick(e, 'login')}} inverted={!fixed}>
                            Log in
                        </Button>
                        <Button as='a' onClick={(e)=>{handleButtonClick(e, 'signup')}}  inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>
                            Sign Up
                        </Button>
                        </Menu.Item>
                    </Container>
            </Menu>
    )
}

export default Navbar
