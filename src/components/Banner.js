/* Heads up!
 * HomepageHeading uses inline styling, however it's not the best practice. Use CSS or styled
 * components for such things.
 */
import PropTypes from 'prop-types'

import {
    Button,
    Container,
    Header,
    Icon,
  } from 'semantic-ui-react';


const Banner = ({ mobile }) => (
    <Container text>
      <Header
        as='h1'
        content='Match-AI'
        inverted
        style={{
          fontSize: mobile ? '2em' : '4em',
          fontWeight: 'normal',
          marginBottom: 0,
          marginTop: mobile ? '1.5em' : '3em',
        }}
      />
      <Header
        as='h2'
        content='Bets and Fastest Way of Matching Yor Data.'
        inverted
        style={{
          fontSize: mobile ? '1.5em' : '1.7em',
          fontWeight: 'normal',
          marginTop: mobile ? '0.5em' : '1.5em',
        }}
      />
      <Button primary size='huge'>
        Get Started
        <Icon name='right arrow' />
      </Button>
    </Container>
  )
  
  Banner.propTypes = {
    mobile: PropTypes.bool,
  }

  export default Banner;