import React from 'react';
import {
    Container,
    Grid,
    Header,
    List,
    Segment,
  } from 'semantic-ui-react';

const Footer = () => {
    return (
        <Segment  vertical style={{ padding: '5em 0em', backgroundColor: '#0A1F30' }}>
            <Container>
                <Grid divided inverted stackable>
                <Grid.Row>
                    <Grid.Column width={3}>
                    <Header inverted as='h4' content='About' />
                    <List link inverted>
                        <List.Item as='a'>Sitemap</List.Item>
                        <List.Item as='a'>Contact Us</List.Item>
                        <List.Item as='a'>Services</List.Item>
                        <List.Item as='a'>Our Team</List.Item>
                    </List>
                    </Grid.Column>
                    <Grid.Column width={3}>
                    <Header inverted as='h4' content='Services' />
                    <List link inverted>
                        <List.Item as='a'>Data Matching</List.Item>
                        <List.Item as='a'>Data Cleaning</List.Item>
                        <List.Item as='a'>Data Deduplication</List.Item>
                        <List.Item as='a'>Feature Engineering</List.Item>
                    </List>
                    </Grid.Column>
                    <Grid.Column width={7}>
                    <Header as='h4' inverted>
                        MatchAI
                    </Header>
                    <p style={{color:"white"}}>
                        We are here to solve your data problems
                    </p>
                    </Grid.Column>
                </Grid.Row>
                </Grid>
            </Container>
        </Segment>
    )
}

export default Footer
