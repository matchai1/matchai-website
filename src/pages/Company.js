import React from 'react';

// import Navbar from '../components/Navbar';
import ResponsiveContainer from '../components/ResponsiveContainer';

const Company = () => {
    return (
        <ResponsiveContainer showBanner={false}>
            Company
        </ResponsiveContainer>
    )
}

export default Company;
