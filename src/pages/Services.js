import React from 'react';

// import Navbar from '../components/Navbar';
import ResponsiveContainer from '../components/ResponsiveContainer';

const Services = () => {
    return (
        <ResponsiveContainer>
            Services
        </ResponsiveContainer>
    )
}

export default Services;
